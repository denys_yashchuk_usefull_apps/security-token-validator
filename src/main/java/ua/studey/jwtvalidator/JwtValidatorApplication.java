package ua.studey.jwtvalidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ua.studey.jwtvalidator.model.Role;
import ua.studey.jwtvalidator.model.User;
import ua.studey.jwtvalidator.repo.UserRepo;

@SpringBootApplication
public class JwtValidatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtValidatorApplication.class, args);
    }

    @Bean
    public UserRepo userRepo() {
        UserRepo userRepo = new UserRepo();
        userRepo.add(new User("test1", "password", Role.ADMIN));
        userRepo.add(new User("test2", "password", Role.ADMIN));
        userRepo.add(new User("test3", "password", Role.USER));
        userRepo.add(new User("test4", "password", Role.USER));
        return userRepo;
    }
}
