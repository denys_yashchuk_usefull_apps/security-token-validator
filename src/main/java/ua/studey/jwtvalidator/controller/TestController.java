package ua.studey.jwtvalidator.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("user/hello")
    public String hello() {
        return "Hello User";
    }

    @GetMapping("user/bye")
    public String bye() {
        return "Bye User";
    }

    @GetMapping("admin/hello")
    public String helloAdm() {
        return "Hello Admin";
    }

    @GetMapping("admin/bye")
    public String byeAdm() {
        return "Bye Admin";
    }

    @GetMapping("/hello")
    public String h() {
        return "Hello";
    }

}
