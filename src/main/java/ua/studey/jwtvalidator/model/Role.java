package ua.studey.jwtvalidator.model;

public enum Role {

    USER, ADMIN;

}
