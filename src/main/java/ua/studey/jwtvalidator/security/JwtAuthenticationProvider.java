package ua.studey.jwtvalidator.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import ua.studey.jwtvalidator.model.User;
import ua.studey.jwtvalidator.security.profiles.JwtAuthenticatedProfile;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final JwtService jwtService;

    @Autowired
    public JwtAuthenticationProvider(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        User user = jwtService.verify(authentication.getCredentials().toString());
        if(user != null){
            JwtAuthenticatedProfile jwtAuthenticatedProfile = new JwtAuthenticatedProfile(user);
            return jwtAuthenticatedProfile;
        } else {
            throw new JwtAuthenticationException("Authentication failed");
        }
    }

    @Override
    public boolean supports(Class<?> authenication) {
        return JwtAuthToken.class.equals(authenication);
    }
}
