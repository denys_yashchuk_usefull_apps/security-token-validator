package ua.studey.jwtvalidator.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.studey.jwtvalidator.model.User;
import ua.studey.jwtvalidator.repo.UserRepo;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Date;

import static java.time.ZoneOffset.UTC;

@Service
public class JwtService {

    @Value("${security.jwt.token.secret-key}")
    private String key;

    @Autowired
    private UserRepo userRepo;

    @PostConstruct
    protected void init() {
        key = Base64.getEncoder().encodeToString(key.getBytes());
    }

    public User verify(String token) {
        Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        return userRepo.getById(Integer.parseInt(claims.getBody().get("sub").toString()));
    }

}
