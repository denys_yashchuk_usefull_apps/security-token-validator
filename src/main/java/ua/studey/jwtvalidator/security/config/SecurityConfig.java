package ua.studey.jwtvalidator.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ua.studey.jwtvalidator.security.JwtAuthenticationEntryPoint;
import ua.studey.jwtvalidator.security.JwtAuthenticationProvider;
import ua.studey.jwtvalidator.security.filter.JwtAuthFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthFilter jwtAuthFilter;
    private final JwtAuthenticationProvider jwtAuthenticationProvider;
    private final JwtAuthenticationEntryPoint jwtAuthEndPoint;

    @Autowired
    public SecurityConfig(JwtAuthFilter jwtAuthFilter, JwtAuthenticationProvider jwtAuthenticationProvider
    , JwtAuthenticationEntryPoint jwtAuthEndPoint) {
        this.jwtAuthFilter = jwtAuthFilter;
        this.jwtAuthenticationProvider = jwtAuthenticationProvider;
        this.jwtAuthEndPoint = jwtAuthEndPoint;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/user/**")
                .hasAnyAuthority("USER", "ADMIN")
                .antMatchers("/admin/**")
                .hasAuthority("ADMIN")
                .antMatchers("/hello")
                .permitAll()
                .and()
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthEndPoint);
    }
}
