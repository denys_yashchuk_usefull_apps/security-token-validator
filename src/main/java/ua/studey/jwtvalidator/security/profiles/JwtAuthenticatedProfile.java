package ua.studey.jwtvalidator.security.profiles;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ua.studey.jwtvalidator.model.User;

import java.util.Collection;
import java.util.Collections;

public class JwtAuthenticatedProfile implements Authentication {

    private final User user;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtAuthenticatedProfile(User user) {
        this.user = user;
        this.authorities = Collections.singleton(new SimpleGrantedAuthority(user.getRole().toString()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return user.getUsername();
    }

    @Override
    public String toString() {
        return "JwtAuthenticatedProfile{" +
                "user=" + user +
                ", authorities=" + authorities +
                '}';
    }
}
